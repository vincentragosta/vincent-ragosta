# README #

WP Scaffold provides a composer based dependency management system for WordPress installation. Based on the [Wordpress Site Stack recipe by Rarst](http://composer.rarst.net/recipe/site-stack)

Theme is inspired by [Sage by Roots](https://roots.io/sage/).

## Getting Started ##

Make sure you are inside of your root project directory. Run the `$ sh start-project.sh` bash script. This will carry you through most of the setup process.

The general idea behind the script is that we sub in a new git configuration on top of a wp theme scaffold. Effectively granting the new git repository access to all of the scaffold files.
    
The two bash script files will be removed once the script completes, comment out the last line in `start-project.sh` to prevent this. 
    
## Local Install ##

Download [Docker](https://hub.docker.com/editions/community/docker-ce-desktop-mac) if it is not already present on your system.

Committed to the repository is a `docker-compose.yml` file. If you have docker installed, simply run `docker-compose up -d`. This will setup your local install on `localhost:8000`.

To stop the instance run `docker-compose down`. 
