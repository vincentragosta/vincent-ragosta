<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * This has been slightly modified (to read environment variables) for use in Docker.
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// IMPORTANT: this file needs to stay in-sync with https://github.com/WordPress/WordPress/blob/master/wp-config-sample.php
// (it gets parsed by the upstream wizard in https://github.com/WordPress/WordPress/blob/f27cb65e1ef25d11b535695a660e7282b98eb742/wp-admin/setup-config.php#L356-L392)

// a helper function to lookup "env_FILE", "env", then fallback
//function getenv_docker($env, $default) {
//    if ($fileEnv = getenv($env . '_FILE')) {
//        return rtrim(file_get_contents($fileEnv), "\r\n");
//    }
//    else if (($val = getenv($env)) !== false) {
//        return $val;
//    }
//    else {
//        return $default;
//    }
//}

define('DB_NAME', 'vincentragosta');
define('DB_USER', 'vincentragosta_user');
define('DB_PASSWORD', 'rapidstrikejustin');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('WP_HOME', 'https://vincentragosta.io/');
define('WP_SITEURL', 'https://vincentragosta.io/wp');
define('WP_CONTENT_DIR', __DIR__ . '/wp-content');
define('WP_CONTENT_URL', 'https://vincentragosta.io/wp-content');
define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');
$table_prefix = 'wp_';

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', getenv_docker('WORDPRESS_DB_NAME', 'wordpress') );

/** MySQL database username */
//define( 'DB_USER', getenv_docker('WORDPRESS_DB_USER', 'example username') );

/** MySQL database password */
//define( 'DB_PASSWORD', getenv_docker('WORDPRESS_DB_PASSWORD', 'example password') );

/**
 * Docker image fallback values above are sourced from the official WordPress installation wizard:
 * https://github.com/WordPress/WordPress/blob/f9cc35ebad82753e9c86de322ea5c76a9001c7e2/wp-admin/setup-config.php#L216-L230
 * (However, using "example username" and "example password" in your database is strongly discouraged.  Please use strong, random credentials!)
 */

/** MySQL hostname */
//define( 'DB_HOST', getenv_docker('WORDPRESS_DB_HOST', 'mysql') );

/** Database Charset to use in creating database tables. */
//define( 'DB_CHARSET', getenv_docker('WORDPRESS_DB_CHARSET', 'utf8') );

/** The Database Collate type. Don't change this if in doubt. */
//define( 'DB_COLLATE', getenv_docker('WORDPRESS_DB_COLLATE', '') );


//define('WP_HOME', 'http://localhost:8000/');
//define('WP_SITEURL', 'http://localhost:8000/wp');
//define('WP_CONTENT_DIR', __DIR__ . '/wp-content');
//define('WP_CONTENT_URL', 'http://localhost:8000/wp-content');
//define('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JJ$;a9Y:mH_(09..L@O+x1zwgB_kNGolj`s5cDmms~7YX,%jxW@3]e BeNIb<ND<');
define('SECURE_AUTH_KEY',  'M!xs+gk]hP/iT&d3]$Z{F=b+c?]2t<TgCKXx5}3$EOpVR??t-eVcFeK8A<4--<JV');
define('LOGGED_IN_KEY',    '|aQCS*tb^hf5bu7%g63@,c:a7$L0:*,==4AW$@cl.|VRP.^G=:?Q-}C3<j1}]#bw');
define('NONCE_KEY',        '{D?A8*P[t,=:NA5Fh*=|3<}{7Tg`Hf8Il]XNJ)hY&n2Z~Y<CN1?b/pl]FFOmm*{q');
define('AUTH_SALT',        'Az&!!Xwn-rF6p,R+Ef2pnPKH<f@|,X=B>/^i?^[=3pa53IXrMXC ~2)|F:oT2ven');
define('SECURE_AUTH_SALT', '+*2ZP3s EsvSX/Nnk32eDC4M{6gRANF&hYzu6,s`i!L|)di-EWgmmxBxUSC`A1N0');
define('LOGGED_IN_SALT',   '?,cu.D{m:6T1?o+r6kf)3pYdx};:ggfsWDty;-+n:s-lISuYq/)7*8Y`L{-|OcxQ');
define('NONCE_SALT',       's);8`H)6K7x`,#^;W&-2S;RhdQ,xy]Z`,V|jXLhm+q:#xj@%`Czb7c*U3L&5X>pN');

//define( 'AUTH_KEY',         getenv_docker('WORDPRESS_AUTH_KEY',         '8fa5470039584139841300feed67daaca3d0f21c') );
//define( 'SECURE_AUTH_KEY',  getenv_docker('WORDPRESS_SECURE_AUTH_KEY',  '18ec735135cf2ca8633cff3b4729663683edd260') );
//define( 'LOGGED_IN_KEY',    getenv_docker('WORDPRESS_LOGGED_IN_KEY',    '28184fad879b9e526742544ce1849df9419ecbe1') );
//define( 'NONCE_KEY',        getenv_docker('WORDPRESS_NONCE_KEY',        '2176f7b5aa258d39cd731ccc5ec15a9e342afc17') );
//define( 'AUTH_SALT',        getenv_docker('WORDPRESS_AUTH_SALT',        '64656ffd148c3edd02040845e1f5cd9cbbbf8869') );
//define( 'SECURE_AUTH_SALT', getenv_docker('WORDPRESS_SECURE_AUTH_SALT', '1d7bfad1f60bf043cc10ffcc820bd19d4161692e') );
//define( 'LOGGED_IN_SALT',   getenv_docker('WORDPRESS_LOGGED_IN_SALT',   '26330a035469a5bd8f29ecfe0a702a32a6d3b39f') );
//define( 'NONCE_SALT',       getenv_docker('WORDPRESS_NONCE_SALT',       '59e0fee9bc2b3dc90b750d8d3b56b82d5817ff16') );
// (See also https://wordpress.stackexchange.com/a/152905/199287)

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
//$table_prefix = getenv_docker('WORDPRESS_TABLE_PREFIX', 'wp_');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
//define( 'WP_DEBUG', !!getenv_docker('WORDPRESS_DEBUG', '') );

// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
    $_SERVER['HTTPS'] = 'on';
}
// (we include this by default because reverse proxying is extremely common in container environments)

//if ($configExtra = getenv_docker('WORDPRESS_CONFIG_EXTRA', '')) {
//    eval($configExtra);
//}

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
