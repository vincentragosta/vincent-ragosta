<?php

use Gilgamesh\Image;
use Theme\Project\Project;

/**
 * @var Project $Project
 */
$Project = Project::createFromGlobal();
?>

<article>
    <div class="container-fluid">
        <div class="wp-block-columns wp-block-columns--full-height in-view" id="page-hero">
            <div class="wp-block-column wp-block-column--vertically-center">
                <div class="title-card in-view">
                    <div class="title-card__image-container">
                        <div class="title-card__image">
                            <div <?= ($featured_image = $Project->featuredImage()) instanceof Image ? 'style="background-image: url(' . $featured_image->url . ');"' : ''; ?>></div>
                        </div>
                    </div>
                    <div class="title-card__content-container">
                        <h1 class="title-card__title typography--xlarge"><?= $Project->title(); ?></h1>
                        <div class="divider title-card__divider">
                            <hr class="divider__line">
                            <hr class="divider__line">
                        </div>
                        <div class="title-card__subtext">
                            <?php if (!empty($project_type = $Project->getProjectTypeName())): ?>
                                <p class="typography--large"><?= $project_type; ?></p>
                            <?php endif; ?>
                            <?php if (!empty($website_url = $Project->website_url)): ?>
                                <a href="<?= $Project->website_url; ?>"
                                   class="button is-style-fill has-white-color has-primary-background-color has-text-color has-background has-border-radius" target="_blank">Visit
                                    Site</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wp-block-columns wp-block-columns--white wp-block-columns--has-bg" id="project-attributes">
            <div class="wp-block-column">
                <?php if (($logo = $Project->logo) instanceof Image): ?>
                    <?= $logo; ?>
                <?php endif; ?>
            </div>
            <?php if ($date_range = $Project->date_range): ?>
                <div class="wp-block-column wp-block-column--vertically-center">
                    <h2 class="typography--medium text--uppercase text--primary" style="margin-bottom: var(--global__sizing__layout-spacing--half);">Date</h2>
                    <p class="typography--large"><?= $date_range; ?></p>
                </div>
            <?php endif; ?>
        </div>
        <?php if (!empty($concept = $Project->concept)): ?>
            <div class="wp-block-columns wp-block-columns--white wp-block-columns--has-bg" id="project-concept">
                <div class="wp-block-column wp-block-column--vertically-center">
                    <h2 class="typography--large text--uppercase"><span class="text--primary text--emphasis">01</span>Concept
                    </h2>
                </div>
                <div class="wp-block-column wp-block-column--vertically-center">
                    <p class="typography--large"><?= $concept; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($development = $Project->development)): ?>
            <div class="wp-block-columns wp-block-columns--white wp-block-columns--has-bg" id="project-development">
                <div class="wp-block-column wp-block-column--vertically-center">
                    <h2 class="typography--large text--uppercase"><span class="text--primary text--emphasis">02</span>Development
                    </h2>
                </div>
                <div class="wp-block-column wp-block-column--vertically-center">
                    <p class="typography--large"><?= $development; ?></p>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($Project->full_page_image instanceof Image || $Project->alt_full_page_image instanceof Image) : ?>
            <div class="wp-block-columns wp-block-columns--white wp-block-columns--has-bg" id="development-images">
                <?php if (($first_image = $Project->full_page_image) instanceof Image): ?>
                    <div class="wp-block-column">
                        <?= $first_image; ?>
                    </div>
                <?php endif; ?>
                <?php if (($second_image = $Project->alt_full_page_image) instanceof Image): ?>
                    <div class="wp-block-column">
                        <?= $second_image; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="wp-block-columns wp-block-columns--white wp-block-columns--has-bg">
            <div class="wp-block-column wp-block-column--horizontally-center">
                <a href="<?= home_url('/projects'); ?>" class="text--uppercase typography--large">Back to Projects</a>
            </div>
        </div>
    </div>
</article>
