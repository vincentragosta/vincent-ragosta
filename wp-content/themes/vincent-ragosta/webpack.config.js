const defaultConfig = require('@wordpress/scripts/config/webpack.config');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const path = require('path');

module.exports = {
    ...defaultConfig,
    mode: 'development',
    entry: {
        frontend: [
            path.resolve(__dirname, '../../plugins/ishtar/assets', 'frontend.js'),
            path.resolve(__dirname, '../../plugins/ereshkigal/assets', 'frontend.js'),
            path.resolve(__dirname, 'assets/', 'frontend.js'),
        ],
        backend: [
            path.resolve(__dirname, '../../plugins/ishtar/assets', 'backend.js'),
            path.resolve(__dirname, '../../plugins/ereshkigal/assets', 'backend.js'),
            path.resolve(__dirname, 'assets/', 'backend.js')
        ]
    },
    module: {
        ...defaultConfig.module,
        rules: [
            ...defaultConfig.module.rules,
            {
                test: /assets\/images\/svg\/.*\.svg$/,
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    spriteFilename: './sprite.svg',
                    runtimeCompat: true
                }
            },
            // {
            //     test: /\.(bmp|png|jpe?g|gif)$/i,
            //     loader: require.resolve( 'file-loader' ),
            //     options: {
            //         name: 'images/[name].[ext]',
            //     },
            // },
            // {
            //     test: /\.(woff|woff2|eot|ttf|otf)$/,
            //     use: [
            //         {
            //             loader: 'file-loader',
            //             options: {
            //                 name: 'fonts/[name].[ext]',
            //             },
            //         },
            //     ],
            // },
        ],
    },
    plugins: [
        ...defaultConfig.plugins,
        new SpriteLoaderPlugin({
            plainSprite: true
        })
    ],
};
