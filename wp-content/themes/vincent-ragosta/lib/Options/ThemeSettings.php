<?php

namespace Theme\Options;

use Gilgamesh\Options\OptionsPage;
use Gilgamesh\Social\SocialIconCollection;
use Theme\PokemonCard\PokemonCard;

/**
 * Class ThemeSettings
 * @package Theme\Options
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $option_name
 * @property string $parent
 * @property string $page_title
 */
final class ThemeSettings extends OptionsPage
{
    const OPTION_NAME = 'theme_settings';
    const PAGE_TITLE = 'Theme Settings';

    public function __construct()
    {
        add_action('fm_submenu_' . static::OPTION_NAME, [$this, 'addFields']);
    }

    public function addFields() {
        try {
            $grades = [];
            foreach (get_terms(['taxonomy' => PokemonCard::GRADE_TAXONOMY]) as $Term) {
                $grades[$Term->term_id] = $Term->name;
            }
            $fm = new \Fieldmanager_Group([
                'name' => static::OPTION_NAME,
                'tabbed' => 'vertical',
                'children' => apply_filters('vincentragosta/fm-theme-settings', [
                    'general' => new \Fieldmanager_Group([
                        'label' => 'General',
                        'children' => [
                            'logo' => new \Fieldmanager_Media('Logo'),
                            'social' => new \Fieldmanager_Group([
                                'label' => 'Social Icons',
                                'limit' => 0,
                                'add_more_label' => 'Add Social Icon',
                                'children' => [
                                    'icon' => new \Fieldmanager_Select('Icon', ['options' => ['Facebook', 'Twitter', 'Instagram', 'LinkedIn']]),
                                    'url' => new \Fieldmanager_Link('Url')
                                ]
                            ])
                        ]
                    ])
                ])
            ]);
            $fm->activate_submenu_page();
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
    }

    public static function socialIcons()
    {
        return new SocialIconCollection(static::getOption('general', 'social'));
    }
}
