<?php

namespace Theme\PokemonCard;

use Gilgamesh\Post\PostRepository;

/**
 * Class PokemonCardRepository
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardRepository extends PostRepository
{
    protected $model_class = PokemonCard::class;

    public function findByPriceSetAndImageUrl(string $price, string $set, string $image_url)
    {
        if (empty($price) || empty($set) || empty($image_url)) {
            return [];
        }
        return $this->find([
            'meta_query' => [
                'relation' => 'AND',
                [
                    'key' => 'price',
                    'value' => $price,
                    'compare' => '='
                ],
                [
                    'key' => 'image_url',
                    'value' => $image_url,
                    'compare' => '='
                ]
            ],
            'tax_query' => [
                [
                    'taxonomy' => PokemonCard::SET_TAXONOMY,
                    'field'    => 'slug',
                    'terms'    => $set,
                ]
            ]
        ]) ?: [];
    }
}
