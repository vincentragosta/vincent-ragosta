<?php

namespace Theme\PokemonCard;

use Gilgamesh\DTO\DTO;
use Gilgamesh\DTO\DTOCollection;

/**
 * Class PokemonCardDTOCollection
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PokemonCardDTOCollection extends DTOCollection
{
    protected static $object_class_name = PokemonCardDTO::class;

    /**
     * @param PokemonCard $item
     * @return DTO
     */
    protected function createDTO($item): DTO
    {
        return new PokemonCardDTO($item);
    }
}
