<?php

namespace Theme\PokemonCard;

use Gilgamesh\Post\Post;

/**
 * Class PokemonCard
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $quantity
 * @property string $last_updated
 * @property string $price
 * @property bool $is_holo
 * @property bool $is_first_edition
 * @property \WP_Term $grade
 * @property string $grade_id
 * @property \WP_Term $set
 * @property string $set_id
 * @property string $image_url
 *
 * @property array $pokemon
 */
class PokemonCard extends Post
{
    const POST_TYPE = 'pokemon-card';
    const GRADE_TAXONOMY = 'grade';
    const SET_TAXONOMY = 'set';

    protected function getGradeId()
    {
        $Grade = $this->grade[0];
        return ($Grade instanceof \WP_Term) ? $Grade->term_id : '';
    }

    protected function getSetId()
    {
        $Set = $this->set[0];
        return ($Set instanceof \WP_Term) ? $Set->term_id : '';
    }
}
