<?php

namespace Theme\PokemonCard;

/**
 * Class PokemonCardCSVParser
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $file_path
 * @property array $file_data
 */
class PokemonCardCSVParser
{
    const NEW_LINE_CHARACTERS = ["\r\n", "\n\r", "\n", "\r"];
    const DELIMITER = ',';
    const CHUNK_SIZE = 8;

    protected $file_data;

    public function __construct(string $file_path = '')
    {
        try {
            if (empty($raw_file_contents = file_get_contents(get_attached_file($file_path)))) {
                throw new PokemonCardException('There were no file contents in the file path provided.', 400);
            }
            if (empty($pruned_file_contents = str_replace(static::NEW_LINE_CHARACTERS, static::DELIMITER, $raw_file_contents))) {
                throw new PokemonCardException('There was an issue pruning the file contents.', 400);
            }
            if (empty($file_contents_as_array = str_getcsv($pruned_file_contents))) {
                throw new PokemonCardException('There was an issue converting the file to an array.', 400);
            }
            if (count($file_contents_as_array) > static::CHUNK_SIZE) {
                $file_contents_as_array = array_chunk($file_contents_as_array, static::CHUNK_SIZE);
            }
            $this->file_data = $file_contents_as_array;
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
    }

    public function getParsedData(string $count = '')
    {
        if (empty($this->file_data)) {
            return [];
        }
        if (!empty($count)) {
            return array_slice($this->file_data, 0, $count);
        }
        return $this->file_data;
    }
}
