<?php

namespace Theme\PokemonCard;

/**
 * Class PokemonCardException
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardException extends \Exception {}
