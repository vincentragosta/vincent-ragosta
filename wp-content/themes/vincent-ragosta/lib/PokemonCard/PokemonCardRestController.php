<?php

namespace Theme\PokemonCard;

use Gilgamesh\Controller\REST\RestController;

/**
 * Class PokemonCardRestController
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardRestController extends RestController
{
    protected $namespace = 'pokemon-cards';

    public function registerRoutes()
    {
        $this->addCreateRoute('/find', [$this, 'findCards']);
    }

    public function findCards(\WP_Rest_Request $request)
    {
        $params = $request->get_params();
        if (empty($params)) {
            throw new PokemonCardException('No params were sent in the request.', 400);
        }
        $query = [
            'post_type' => PokemonCard::POST_TYPE,
            'post_status' => ['publish'],
            'posts_per_page' => $params['per_page'] ?: 24,
            'order' => $params['order'] ?: 'desc',
            'paged' => $params['page'] ?: 1,
            'meta_key' => $params['meta_key'] ?: '',
            'orderby' => $params['orderby'] ?: '',
        ];
        if (!empty($params['grade']) && !empty($params['set'])) {
            $query['tax_query'] = [
                [
                    'taxonomy' => PokemonCard::GRADE_TAXONOMY,
                    'terms' => $params['grade']
                ],
                [
                    'taxonomy' => PokemonCard::SET_TAXONOMY,
                    'terms' => $params['set']
                ]
            ];
        } else if (!empty($params['grade']) && empty($params['set'])) {
            $query['tax_query'] = [
                [
                    'taxonomy' => PokemonCard::GRADE_TAXONOMY,
                    'terms' => $params['grade']
                ]
            ];
        } elseif (empty($params['grade']) && !empty($params['set'])) {
            $query['tax_query'] = [
                [
                    'taxonomy' => PokemonCard::SET_TAXONOMY,
                    'terms' => $params['set'],
                ]
            ];
        }
        if (!empty($search = $params['s'])) {
            $query['s'] = $search;
        }
        $query = new \WP_Query($query);
        if (empty($cards = $query->posts)) {
            return new \WP_REST_Response([], 200);
        }
        $cards = array_map(function ($card) {
            $PokemonCard = new PokemonCard($card);
            /** @var PokemonCard $PokemonCard */
            return [
                'quantity' => $PokemonCard->quantity,
                'last_updated' => $PokemonCard->last_updated,
                'price' => $PokemonCard->price,
                'is_holo' => $PokemonCard->is_holo,
                'is_first_edition' => $PokemonCard->is_first_edition,
                'grade_id' => $PokemonCard->grade_id,
                'set_id' => $PokemonCard->set_id,
                'image_url' => $PokemonCard->image_url
            ];
        }, $cards);
        $Response = new \WP_REST_Response($cards, 200);
        $Response->header('X-WP-Total', $query->max_num_pages);
        $Response->header('X-WP-TotalPages', $query->found_posts);
        return $Response;
    }
}
