<?php

namespace Theme\PokemonCard;

use Gilgamesh\Object\ObjectCollection;

/**
 * Class PokemonCardRowCollection
 * @package Theme\PokemonCard
 */
class PokemonCardRowCollection extends ObjectCollection
{
    protected static $object_class_name = PokemonCardRow::class;

    public function __construct(array $items)
    {
        if (!$items[0] instanceof PokemonCardRow) {
            $items = array_map([PokemonCardRow::class, 'createFromCSVRow'], $items);
        }
        parent::__construct($items);
    }

    public function getObjectHash($item)
    {
        /** @var PokemonCardRow $item */
        return md5($item->getTitle() . $item->getSetName() . $item->getPrice());
    }
}
