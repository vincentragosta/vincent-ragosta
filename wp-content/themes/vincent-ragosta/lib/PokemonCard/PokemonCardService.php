<?php

namespace Theme\PokemonCard;

use Gilgamesh\Image;
use Gilgamesh\Service\Service;

/**
 * Class PokemonCardService
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class PokemonCardService extends Service
{
    public function __construct($items)
    {
        if (!$items instanceof PokemonCardRowCollection) {
            $items = (new PokemonCardRowCollection($items))->getAll();
        }
        parent::__construct($items);
    }

    public function run()
    {
        foreach ($this->items as $PokemonCardRow) {
            /** @var PokemonCardRow $PokemonCardRow */
            if ($this->cardExists($PokemonCardRow)) {
                continue;
            }
            /** @var PokemonCard $PokemonCard */
            $PokemonCard = $this->createPokemonCardFromPokemonCardRow($PokemonCardRow);
            $PokemonCard->post_status = 'publish';
            $Grade = (new PokemonCardGradeFactory($PokemonCard->price))->create();
            $PokemonCard->grade = $this->getGradeId($Grade);
            $PokemonCard->set = $this->getSetId($PokemonCardRow->getSetName());
            (new PokemonCardRepository())->add($PokemonCard);
        }
    }

    public function createPokemonCardFromPokemonCardRow(PokemonCardRow $PokemonCardRow)
    {
        $PokemonCard = new PokemonCard();
        $PokemonCard->post_title = $PokemonCardRow->getTitle();
        $PokemonCard->post_name = $this->getCardSlug($PokemonCardRow->getSetName(), $PokemonCardRow->getTitle());
        $PokemonCard->price = $PokemonCardRow->getPrice();
        $PokemonCard->quantity = $PokemonCardRow->getQuantity();
        $PokemonCard->is_holo = $PokemonCardRow->isHolo();
        $PokemonCard->is_first_edition = $PokemonCardRow->isFirstEdition();
        $PokemonCard->last_updated = $PokemonCardRow->getLastUpdated();
        $PokemonCard->image_url = $PokemonCardRow->getImageUrl();

        /* For grouped display in the CMS */
        $PokemonCard->pokemon = [
            'price' => $PokemonCardRow->getPrice(),
            'quantity' => $PokemonCardRow->getQuantity(),
            'is_holo' => $PokemonCardRow->isHolo(),
            'is_first_edition' => $PokemonCardRow->isFirstEdition(),
            'last_updated' => $PokemonCardRow->getLastUpdated(),
            'image_url' => $PokemonCardRow->getImageUrl()
        ];

        $PokemonCard->setFeaturedImage(Image::create_from_url($PokemonCardRow->getImageUrl()));
        return $PokemonCard;
    }

    public function cardExists(PokemonCardRow $Row)
    {
        return !empty((new PokemonCardRepository())->findByPriceSetAndImageUrl($Row->getPrice(), $Row->getSetSlug(), $Row->getImageUrl()));
    }

    public function getGradeId(PokemonCardGrade $Grade)
    {
        $grade_id = '';
        if (is_null(term_exists($Grade->getSlug()))) {
            $result = wp_insert_term($Grade->getLabel(), PokemonCard::GRADE_TAXONOMY, [
                'description' => $Grade->getDescription(),
                'slug' => $Grade->getSlug()
            ]);
            if (!is_wp_error($result)) {
                $grade_id = $result['term_id'];
            }
        } else {
            $Term = get_term_by('slug', $Grade->getSlug(), PokemonCard::GRADE_TAXONOMY);
            $grade_id = $Term->term_id;
        }
        return $grade_id;
    }

    public function getSetId(string $set_name)
    {
        $set_id = '';
        if (is_null(term_exists($set_name))) {
            $result = wp_insert_term($set_name, PokemonCard::SET_TAXONOMY, [
                'description' => 'The booster pack that this card was obtained in.',
                'slug' => strtolower($set_name)
            ]);
            if (!is_wp_error($result)) {
                $set_id = $result['term_id'];
            }
        } else {
            $Term = get_term_by('name', $set_name, PokemonCard::SET_TAXONOMY);
            $set_id = $Term->term_id;
        }
        return $set_id;
    }

    protected function getCardSlug(string $set_name, string $title)
    {
        return strtolower(str_replace(' ', '-', sprintf('%s-%s', $set_name, $title)));
    }
}
