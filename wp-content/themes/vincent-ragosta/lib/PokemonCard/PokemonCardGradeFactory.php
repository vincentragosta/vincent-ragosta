<?php

namespace Theme\PokemonCard;

/**
 * Class PokemonCardGradeFactory
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $price
 */
class PokemonCardGradeFactory
{
    const GRADE_1 = 1;
    const GRADE_2 = 2;
    const GRADE_3 = 3;
    const GRADE_4 = 4;
    const GRADE_5 = 5;
    const GRADE_6 = 6;
    const GRADE_7 = 7;
    const GRADE_8 = 8;

    const GRADE_1_SLUG = 'one';
    const GRADE_2_SLUG = 'two';
    const GRADE_3_SLUG = 'three';
    const GRADE_4_SLUG = 'four';
    const GRADE_5_SLUG = 'five';
    const GRADE_6_SLUG = 'six';
    const GRADE_7_SLUG = 'seven';
    const GRADE_8_SLUG = 'eight';

    const GRADE_1_PRICE_UPPER = 10.00;
    const GRADE_2_PRICE_UPPER = 20.00;
    const GRADE_3_PRICE_UPPER = 30.00;
    const GRADE_4_PRICE_UPPER = 40.00;
    const GRADE_5_PRICE_UPPER = 50.00;
    const GRADE_6_PRICE_UPPER = 60.00;
    const GRADE_7_PRICE_UPPER = 75.00;

    const GRADE_1_LABEL = self::GRADE_1 . ': Below $' . self::GRADE_1_PRICE_UPPER;
    const GRADE_2_LABEL = self::GRADE_2 . ': $' . self::GRADE_1_PRICE_UPPER . ' - ' . self::GRADE_2_PRICE_UPPER;
    const GRADE_3_LABEL = self::GRADE_3 . ': $' . self::GRADE_2_PRICE_UPPER . ' - ' . self::GRADE_3_PRICE_UPPER;
    const GRADE_4_LABEL = self::GRADE_4 . ': $' . self::GRADE_3_PRICE_UPPER . ' - ' . self::GRADE_4_PRICE_UPPER;
    const GRADE_5_LABEL = self::GRADE_5 . ': $' . self::GRADE_4_PRICE_UPPER . ' - ' . self::GRADE_5_PRICE_UPPER;
    const GRADE_6_LABEL = self::GRADE_6 . ': $' . self::GRADE_5_PRICE_UPPER . ' - ' . self::GRADE_6_PRICE_UPPER;
    const GRADE_7_LABEL = self::GRADE_7 . ': $' . self::GRADE_6_PRICE_UPPER . ' - ' . self::GRADE_7_PRICE_UPPER;
    const GRADE_8_LABEL = self::GRADE_8 . ': Above $' . self::GRADE_7_PRICE_UPPER;

    const GRADE_1_DESCRIPTION = 'Price of the pokemon card is below $10.00.';
    const GRADE_2_DESCRIPTION = 'Price of the pokemon card is between $10.00 and $20.00.';
    const GRADE_3_DESCRIPTION = 'Price of the pokemon card is between $20.00 and $30.00.';
    const GRADE_4_DESCRIPTION = 'Price of the pokemon card is between $30.00 and $40.00.';
    const GRADE_5_DESCRIPTION = 'Price of the pokemon card is between $40.00 and $50.00.';
    const GRADE_6_DESCRIPTION = 'Price of the pokemon card is between $50.00 and $60.00.';
    const GRADE_7_DESCRIPTION = 'Price of the pokemon card is between $60.00 and $75.00.';
    const GRADE_8_DESCRIPTION = 'Price of the pokemon card is greater than $75.00';

    protected $price;

    public function __construct(string $price)
    {
        $this->price = $price;
    }

    /**
     * TODO: consider making a switch statement
     */
    public function create()
    {
        $Grade = new PokemonCardGrade(static::GRADE_1_LABEL, static::GRADE_1_DESCRIPTION, static::GRADE_1_SLUG);
        if ($this->price >= static::GRADE_1_PRICE_UPPER && $this->price <= static::GRADE_2_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_2_LABEL, static::GRADE_2_DESCRIPTION, static::GRADE_2_SLUG);
        } else if ($this->price > static::GRADE_2_PRICE_UPPER && $this->price <= static::GRADE_3_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_3_LABEL, static::GRADE_3_DESCRIPTION, static::GRADE_3_SLUG);
        } else if ($this->price > static::GRADE_3_PRICE_UPPER && $this->price <= static::GRADE_4_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_4_LABEL, static::GRADE_4_DESCRIPTION, static::GRADE_4_SLUG);
        } else if ($this->price > static::GRADE_4_PRICE_UPPER && $this->price <= static::GRADE_5_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_5_LABEL, static::GRADE_5_DESCRIPTION, static::GRADE_5_SLUG);
        } else if ($this->price > static::GRADE_5_PRICE_UPPER && $this->price <= static::GRADE_6_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_6_LABEL, static::GRADE_6_DESCRIPTION, static::GRADE_6_SLUG);
        } else if ($this->price > static::GRADE_6_PRICE_UPPER && $this->price <= static::GRADE_7_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_7_LABEL, static::GRADE_7_DESCRIPTION, static::GRADE_7_SLUG);
        } else if ($this->price > static::GRADE_7_PRICE_UPPER) {
            $Grade = new PokemonCardGrade(static::GRADE_8_LABEL, static::GRADE_8_DESCRIPTION, static::GRADE_8_SLUG);
        }
        return $Grade;
    }
}
