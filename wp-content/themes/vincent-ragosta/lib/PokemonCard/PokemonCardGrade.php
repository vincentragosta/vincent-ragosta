<?php

namespace Theme\PokemonCard;

/**
 * Class PokemonCardGrade
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $label
 * @property string $description
 * @property string $slug
 */
class PokemonCardGrade
{
    protected $label;
    protected $description;
    protected $slug;

    public function __construct(string $label, string $description, string $slug)
    {
        $this->label = $label;
        $this->description = $description;
        $this->slug = $slug;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getSlug()
    {
        return $this->slug;
    }
}
