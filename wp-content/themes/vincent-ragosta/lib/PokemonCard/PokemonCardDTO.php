<?php

namespace Theme\PokemonCard;

use Gilgamesh\DTO\DTO;

/**
 * Class PokemonCardDTO
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $quantity
 * @property string $last_updated
 * @property string $price
 * @property string $is_holo
 * @property string $is_first_edition
 * @property string $grade_id
 * @property string $set_id
 * @property string $image_url
 */
class PokemonCardDTO extends DTO
{
    protected $quantity;
    protected $last_updated;
    protected $price;
    protected $is_holo;
    protected $is_first_edition;
    protected $grade_id;
    protected $set_id;
    protected $image_url;

    public function __construct(PokemonCard $PokemonCard)
    {
        $this->quantity = $PokemonCard->quantity;
        $this->last_updated = $PokemonCard->last_updated;
        $this->price = $PokemonCard->price;
        $this->is_holo = $PokemonCard->is_holo;
        $this->is_first_edition = $PokemonCard->is_first_edition;
        $this->grade_id = $PokemonCard->grade_id;
        $this->set_id = $PokemonCard->set_id;
    }
}
