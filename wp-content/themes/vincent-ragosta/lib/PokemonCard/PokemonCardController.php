<?php

namespace Theme\PokemonCard;

use Gilgamesh\Image;
use Theme\Options\ThemeSettings;

/**
 * Class PokemonCardController
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardController
{
    public function __construct()
    {
        add_action('current_screen', [$this, 'runParser']);
        add_filter('use_block_editor_for_post_type', [$this, 'disableGutenberg'], 10, 2);
        add_action($this->getFieldManagerHook(), [$this, 'addFields']);
        add_filter('gilgamesh/admin_col/quantity/pokemon-card', [$this, 'getQuantity'], 10, 2);
        add_filter('gilgamesh/admin_col/is_holo/pokemon-card', [$this, 'getIsHolo'], 10, 2);
        add_filter('gilgamesh/admin_col/is_first_edition/pokemon-card', [$this, 'getIsFirstEdition'], 10, 2);
        add_action('rest_api_init', [$this, 'updateRestApi']);
        add_filter('global_js_vars', [$this, 'addJsVars'], 1, 1);
        add_filter('vincentragosta/fm-theme-settings', [$this, 'updateFieldManagerThemeSettings']);
    }

    public function runParser($current_screen)
    {
        if ($current_screen->id === $this->getSettingsPage()) {
            if (!empty($csv_file = ThemeSettings::getOption('pokemon', 'csv')) && ThemeSettings::getOption('pokemon', 'sync')) {
                (new PokemonCardService((new PokemonCardCSVParser($csv_file))->getParsedData()))->run();
            }
        }
    }

    public function disableGutenberg($current_status, $post_type)
    {
        if ($post_type === PokemonCard::POST_TYPE) {
            $current_status = false;
        }
        return $current_status;
    }

    public function addFields()
    {
        try {
            $fm = new \Fieldmanager_Group('Pokemon', [
                'name' => 'pokemon',
                'children' => [
                    'price' => new \Fieldmanager_TextField('Price', [
                        'description' => 'The current price of the card.'
                    ]),
                    'last_updated' => new \Fieldmanager_TextField('Last Updated', [
                        'description' => 'The last time the price was updated.'
                    ]),
                    'quantity' => new \Fieldmanager_TextField('Quantity'),
                    'is_holo' => new \Fieldmanager_Checkbox('Is Holo?'),
                    'is_first_edition' => new \Fieldmanager_Checkbox('Is First Edition?'),
                    'image_url' => new \Fieldmanager_TextField('TCGPlayer Image Url')
                ]
            ]);
            $fm->add_meta_box('Pokemon Fields', [PokemonCard::POST_TYPE]);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    public function getQuantity($content, $post_id)
    {
        /** @var PokemonCard $PokemonCard */
        $PokemonCard = (new PokemonCardRepository())->findById($post_id);
        return $PokemonCard->quantity;
    }

    public function getIsHolo($content, $post_id)
    {
        /** @var PokemonCard $PokemonCard */
        $PokemonCard = (new PokemonCardRepository())->findById($post_id);
        return $PokemonCard->is_holo ? 'Yes' : 'No';
    }

    public function getIsFirstEdition($content, $post_id)
    {
        /** @var PokemonCard $PokemonCard */
        $PokemonCard = (new PokemonCardRepository())->findById($post_id);
        return $PokemonCard->is_first_edition ? 'Yes' : 'No';
    }

    public function updateRestApi() {
        register_rest_field(PokemonCard::POST_TYPE, 'price', [
            'get_callback' => function ($post) {
                return (new PokemonCard($post['id']))->price;
            },
            'update_callback' => function ($value, $post) {
                $PokemonCard = new PokemonCard($post);
                $PokemonCard->price = $value;
                return (new PokemonCardRepository())->add($PokemonCard);
            },
            'schema' => [
                'description' => __('The card price.'),
                'type' => 'string'
            ],
        ]);

        register_rest_field(PokemonCard::POST_TYPE, PokemonCard::GRADE_TAXONOMY, [
            'get_callback' => function ($post) {
                return (new PokemonCard($post['id']))->grade[0];
            },
            'update_callback' => function ($value, $post) {
                $PokemonCard = new PokemonCard($post);
                $PokemonCard->grade = $value;
                return (new PokemonCardRepository())->add($PokemonCard);
            },
            'schema' => [
                'description' => __('The card grade.'),
                'type' => 'string'
            ],
        ]);

        register_rest_field(PokemonCard::POST_TYPE, PokemonCard::SET_TAXONOMY, [
            'get_callback' => function ($post) {
                return (new PokemonCard($post['id']))->set[0];
            },
            'update_callback' => function ($value, $post) {
                $PokemonCard = new PokemonCard($post);
                $PokemonCard->set = $value;
                return (new PokemonCardRepository())->add($PokemonCard);
            },
            'schema' => [
                'description' => __('The card booster pack.'),
                'type' => 'string'
            ],
        ]);

        register_rest_field(PokemonCard::POST_TYPE, 'image_url', [
            'get_callback' => function ($post) {
                return ($image = (new PokemonCard($post['id']))->featuredImage()) instanceof Image ? $image->url : '';
            },
            'update_callback' => function ($value, $post) {
                $PokemonCard = new PokemonCard($post);
                $PokemonCard->setFeaturedImage(Image::create_from_url($value));
                return (new PokemonCardRepository())->add($PokemonCard);
            },
            'schema' => [
                'description' => __('The featured image url.'),
                'type' => 'string'
            ],
        ]);
    }

    public function updateFieldManagerThemeSettings($config)
    {
        try {
            $config = array_merge($config, [
                'pokemon' => new \Fieldmanager_Group([
                    'label' => 'Pokemon',
                    'children' => [
                        'csv' => new \Fieldmanager_Media('Pokemon CSV'),
                        'sync' => new \Fieldmanager_Checkbox('Turn on Sync')
                    ]
                ])
            ]);
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
        return $config;
    }

    protected function getFieldManagerHook()
    {
        return sprintf('fm_post_%s', PokemonCard::POST_TYPE);
    }

    protected function getSettingsPage()
    {
        return sprintf('settings_page_%s', ThemeSettings::OPTION_NAME);
    }
}
