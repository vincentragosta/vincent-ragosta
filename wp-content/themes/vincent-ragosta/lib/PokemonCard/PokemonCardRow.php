<?php

namespace Theme\PokemonCard;

/**
 * Class PokemonCardRow
 * @package Theme\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $price
 * @property string $set_name
 * @property string $quantity
 * @property bool $is_holo
 * @property bool $is_first_edition
 * @property string $last_updated
 * @property string $image_url
 */
class PokemonCardRow
{
    const CHECKED = 'Yes';

    protected $title;
    protected $price;
    protected $set_name;
    protected $quantity;
    protected $is_holo;
    protected $is_first_edition;
    protected $last_updated;
    protected $image_url;

    public function __construct(string $title, string $set_name, string $price, string $quantity, string $is_holo, string $is_first_edition, string $last_updated, string $image_url)
    {
        $this->title = $title;
        $this->set_name = $set_name;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->is_holo = $is_holo === static::CHECKED;
        $this->is_first_edition = $is_first_edition === static::CHECKED;
        $this->last_updated = $last_updated;
        $this->image_url = $image_url;
    }

    public static function createFromCSVRow(array $data)
    {
        list($title, $set_name, $price, $quantity, $is_holo, $is_first_edition, $last_updated, $image_url) = $data;
        return new static($title, $set_name, $price, $quantity, $is_holo, $is_first_edition, $last_updated, $image_url);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSetSlug(): string
    {
        return strtolower(
            str_replace(' ', '-',
                str_replace(' - ', ' ',
                    str_replace(':', '', $this->set_name)
                )
            )
        );
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function getSetName(): string
    {
        return $this->set_name;
    }

    public function getQuantity(): string
    {
        return $this->quantity;
    }

    public function isHolo(): bool
    {
        return $this->is_holo;
    }

    public function isFirstEdition(): bool
    {
        return $this->is_first_edition;
    }

    public function getLastUpdated(): string
    {
        return $this->last_updated;
    }

    public function getImageUrl(): string
    {
        return $this->image_url;
    }
}
