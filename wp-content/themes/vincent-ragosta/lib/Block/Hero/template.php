<?php
/**
 * Expected:
 * @var string $title
 * @var string $subtext
 * @var Image $image
 * @var string $alternate_title
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Gilgamesh\Image;
use Ishtar\Utility\ComponentUtility;
use Theme\Block\Divider\DividerView;
use Theme\Components\Moon\MoonView;

?>

<div <?= ComponentUtility::attributes('hero', $class_modifiers, $element_attributes); ?>>
    <div class="hero__column">
        <?php if (!empty($title)): ?>
            <h2 class="hero__title typography--xlarge text--uppercase"><?= $title; ?></h2>
            <?= (new DividerView())->elementAttributes(['class' => 'hero__divider']); ?>
        <?php endif; ?>
        <?php if (!empty($subtext)): ?>
            <div class="hero__subtext">
                <?= $subtext; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="hero__column">
        <?= (new MoonView($image, $alternate_title))->elementAttributes(['class' => 'hero__moon']); ?>
    </div>
</div>
