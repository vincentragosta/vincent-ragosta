<?php

namespace Theme\Block\Hero;

use Gilgamesh\Image;
use Ishtar\View\ComponentView;

/**
 * Class HeroView
 * @package Theme\Block\Hero
 * @author Vincent Ragosata <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $subtext
 * @property Image $image
 * @property string $alternate_title
 * @property string $id
 */
final class HeroView extends ComponentView
{
    protected $name = 'template';
    protected static $default_properties = [
        'title' => '',
        'subtext' => '',
        'image' => null,
        'alternate_title' => '',
        'id' => '',
    ];

    public function __construct(string $title, string $subtext, $image = '', $alternate_title = '', string $id = '')
    {
        parent::__construct(compact('title', 'subtext', 'image', 'alternate_title', 'id'));
        $this->elementAttributes($this->getHeroAttributes());
    }

    protected function getHeroAttributes()
    {
        $element_attributes = [
            'class' => 'wp-block-columns wp-block-columns--full-height'
        ];
        if ($this->id) {
            $element_attributes['id'] = $this->id;
        }
        return $element_attributes;
    }
}
