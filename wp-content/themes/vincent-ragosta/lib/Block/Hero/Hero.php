<?php

namespace Theme\Block\Hero;

use Ereshkigal\Block\Block;
use Gilgamesh\Image;
use Ishtar\View\View;

/**
 * Class Hero
 * @package Theme\Block\Hero
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class Hero extends Block
{
    protected $name = 'hero';
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View(
            $attributes['title'] ?: '',
            $attributes['subtext'] ?: '',
            !empty($image = $attributes['imageId']) ? Image::get_by_attachment_id($image) : '',
            $attributes['alternateTitle'],
            $attributes['id'] ?: '',
        );
    }
}
