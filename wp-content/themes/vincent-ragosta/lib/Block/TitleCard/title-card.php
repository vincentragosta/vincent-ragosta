<?php
/**
 * Expected:
 * @var string $title
 * @var string $subtext
 * @var Image $image
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Gilgamesh\Image;
use Ishtar\Utility\ComponentUtility;
use Theme\Block\Divider\DividerView;

?>

<div <?= ComponentUtility::attributes('title-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($image instanceof Image): ?>
        <div class="title-card__image-container">
            <div class="title-card__image">
                <div style="background-image: url(<?= $image->url; ?>);" ></div>
            </div>
        </div>
    <?php endif; ?>
    <div class="title-card__content-container">
        <?php if (!empty($title)): ?>
            <h2 class="title-card__title typography--xlarge"><?= $title; ?></h2>
            <?= (new DividerView())->elementAttributes(['class' => 'title-card__divider']); ?>
        <?php endif; ?>
        <?php if (!empty($subtext)): ?>
            <div class="title-card__subtext">
                <?= $subtext; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
