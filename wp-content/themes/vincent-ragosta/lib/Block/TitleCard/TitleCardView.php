<?php

namespace Theme\Block\TitleCard;

use Gilgamesh\Image;
use Ishtar\View\ComponentView;

/**
 * Class TitleCardView
 * @package Theme\Block\TitleCard
 * @author Vincent Ragosata <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $subtext
 * @property Image $image
 */
class TitleCardView extends ComponentView
{
    protected $name = 'title-card';
    protected static $default_properties = [
        'title' => '',
        'subtext' => '',
        'image' => null
    ];

    public function __construct(string $title, string $subtext, $image = '')
    {
        parent::__construct(compact('title', 'subtext', 'image'));
    }
}
