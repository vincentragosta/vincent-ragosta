<?php

namespace Theme\Block\TitleCard;

use Ereshkigal\Block\Block;
use Gilgamesh\Image;
use Ishtar\View\View;
use Theme\Project\Project;

/**
 * Class TitleCard
 * @package Theme\Block\TitleCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class TitleCard extends Block
{
    protected $name = 'title-card';
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        /** @var Project $Project */
        $View = $this->getView();
        return new $View(
            $attributes['title'] ?: '',
            $attributes['subtext'] ?: '',
            !empty($image = $attributes['imageId']) ? Image::get_by_attachment_id($image) : ''
        );
    }
}
