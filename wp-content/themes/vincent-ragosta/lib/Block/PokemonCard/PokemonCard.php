<?php

namespace Theme\Block\PokemonCard;

use Ereshkigal\Block\Block;
use Ishtar\View\View;
use Theme\PokemonCard\PokemonCardRepository;

/**
 * Class PokemonCard
 * @package Theme\Block\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class PokemonCard extends Block
{
    protected $name = 'pokemon-card';
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View((new PokemonCardRepository())->findById($attributes['picks']));
    }
}
