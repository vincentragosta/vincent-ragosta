<?php

namespace Theme\Block\PokemonCard;

use Gilgamesh\Image;
use Ishtar\View\ComponentView;
use Theme\PokemonCard\PokemonCard;

/**
 * Class PokemonCardView
 * @package Theme\Block\PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $price
 * @property string $last_updated
 * @property string $quantity
 * @property Image $Image
 */
final class PokemonCardView extends ComponentView
{
    protected $name = 'pokemon-card';
    protected static $default_properties = [
        'title' => '',
        'price' => '',
        'last_updated' => '',
        'quantity' => '',
        'Image' => null
    ];

    public function __construct(PokemonCard $PokemonCard)
    {
        parent::__construct([
            'title' => $PokemonCard->title(),
            'price' => $PokemonCard->price,
            'last_updated' => $PokemonCard->last_updated,
            'quantity' => $PokemonCard->quantity,
            'Image' => $PokemonCard->featuredImage()
        ]);
        if ($this->Image instanceof Image) {
            $this->Image->width(200)->height(277);
        }
    }
}
