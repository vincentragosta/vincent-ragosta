<?php
/**
 * Expected:
 * @var string $title
 * @var string $price
 * @var string $last_updated
 * @var string $quantity
 * @var string $grade
 * @var string $set
 * @var Image $Image
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Gilgamesh\Image;
use Ishtar\Utility\ComponentUtility;

?>

<div <?= ComponentUtility::attributes('pokemon-card', $class_modifiers, $element_attributes); ?>>
    <div class="pokemon-card__image-container">
        <?= $Image->css_class('pokemon-card__image'); ?>
    </div>
</div>
