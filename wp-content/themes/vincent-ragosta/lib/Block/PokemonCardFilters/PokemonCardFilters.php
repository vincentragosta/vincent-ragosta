<?php

namespace Theme\Block\PokemonCardFilters;

use Ereshkigal\Block\Block;
use Ishtar\View\View;

/**
 * Class PokemonCardFilters
 * @package Theme\Block\PokemonCardFilters
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class PokemonCardFilters extends Block
{
    protected $name = 'pokemon-card-filters';
    protected $View = PokemonCardFiltersView::class;
    // consider making filter for this instead
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View($attributes['text'] ?: '');
    }
}
