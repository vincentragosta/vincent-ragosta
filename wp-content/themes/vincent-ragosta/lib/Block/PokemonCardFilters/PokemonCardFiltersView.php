<?php

namespace Theme\Block\PokemonCardFilters;

use Ishtar\View\ComponentView;

/**
 * Class PokemonCardFiltersView
 * @package Theme\Block\PokemonCardFilters
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class PokemonCardFiltersView extends ComponentView
{
    protected $name = 'pokemon-card-filters';
    protected static $default_properties = [];

    public function __construct(string $text)
    {
        parent::__construct(compact('text'));
        $this->elementAttributes(['id' => 'pokemon-card-filters']);
    }
}
