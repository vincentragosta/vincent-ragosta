<?php
/**
 * Expected:
 * @var string $text
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Ishtar\Utility\ComponentUtility;

?>

<div <?= ComponentUtility::attributes('pokemon-card-filters', $class_modifiers, $element_attributes); ?>>
    <div class="pokemon-card-filters__container">
        <?php if (!empty($text)): ?>
            <div class="pokemon-card-filters__content-container">
                <?= $text; ?>
            </div>
        <?php endif; ?>
        <div class="pokemon-card-filters__filters-container"></div>
    </div>
</div>
