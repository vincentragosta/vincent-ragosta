<?php

namespace Theme\Block\ProjectCard;

use Gilgamesh\Image;
use Ishtar\View\ComponentView;
use Theme\Project\Project;

/**
 * Class ProjectCardView
 * @package Theme\Block\ProjectCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $url
 * @property Image $image
 * @property string $type
 */
final class ProjectCardView extends ComponentView
{
    protected $name = 'project-card';
    protected static $default_properties = [
        'title' => '',
        'url' => '',
        'image' => null,
        'type' => ''
    ];

    public function __construct(Project $Project)
    {
        parent::__construct([
            'title' => $Project->title(),
            'url' => $Project->permalink(),
            'image' => $Project->featuredImage(),
            'type' => $Project->getProjectTypeName()
        ]);
        if ($this->image instanceof Image) {
            $this->image = $this->image->width(1570)->height(1200);
        }
        if ($this->type) {
            $this->type = strtolower($this->type);
        }
    }
}
