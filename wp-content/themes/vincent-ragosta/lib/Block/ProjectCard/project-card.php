<?php
/**
 * Expected:
 * @var string $title
 * @var Image $image
 * @var string $type
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Gilgamesh\Image;
use Ishtar\Utility\ComponentUtility;
use Theme\Block\Divider\DividerView;

if (empty($title) || empty($url)) {
    return '';
}

?>

<div <?= ComponentUtility::attributes('project-card', $class_modifiers, $element_attributes); ?> style="position: relative;">
    <div class="project-card__image-container">
        <div class="project-card__image">
            <div style="background-image: url(<?= $image->url; ?>);" ></div>
        </div>
    </div>
    <div class="project-card__content-container">
        <h2 class="typography--xlarge"><?= $title; ?></h2>
        <?= new DividerView(); ?>
        <?php if (!empty($type)): ?>
            <p class="typography--large"><?= $type; ?></p>
        <?php endif; ?>
        <a href="<?= $url; ?>" class="project-card__button button is-style-fill has-white-color has-primary-background-color has-text-color has-background">Show me more</a>
    </div>
</div>
