<?php

namespace Theme\Block\ProjectCard;

use Ereshkigal\Block\Block;
use Gilgamesh\Image;
use Ishtar\Utility\ElementUtility;
use Ishtar\View\View;
use Theme\Block\TitleCard\TitleCardView;
use Theme\Project\Project;
use Theme\Project\ProjectRepository;

/**
 * Class ProjectCard
 * @package Theme\Block\ProjectCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class ProjectCard extends Block
{
    protected $name = 'project-card';
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        /** @var Project $Project */
        $Project = (new ProjectRepository())->findById($attributes['picks']);
        return new TitleCardView(
            $Project->title(),
            $this->getSubTextFromProject($Project),
            ($image = $Project->featuredImage()) instanceof Image ? $image : ''
        );
    }

    protected function getSubTextFromProject(Project $Project)
    {
        $paragraph = !empty($type = $Project->getProjectTypeName()) ? ElementUtility::create('p', strtolower($type), ['class' => 'typography--large']) : '';
        $button = ElementUtility::create('a', 'Show me more', [
            'class' => 'button is-style-fill has-white-color has-primary-background-color has-text-color has-background has-border-radius',
            'href' => $Project->permalink()
        ]);
        return sprintf('%s%s', $paragraph, $button);
    }
}
