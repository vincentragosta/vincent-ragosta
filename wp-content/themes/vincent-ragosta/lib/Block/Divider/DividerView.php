<?php

namespace Theme\Block\Divider;

use Ishtar\View\ComponentView;

/**
 * Class DividerView
 * @package Theme\Block\Divider
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
final class DividerView extends ComponentView
{
    protected $name = 'template';
    protected static $default_properties = [];
}
