<?php
/**
 * Expected:
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Ishtar\Utility\ComponentUtility;

?>

<div <?= ComponentUtility::attributes('divider', $class_modifiers, $element_attributes); ?>>
    <hr class="divider__line" />
    <hr class="divider__line" />
</div>
