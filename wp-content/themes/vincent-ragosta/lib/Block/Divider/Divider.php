<?php

namespace Theme\Block\Divider;

use Ereshkigal\Block\Block;
use Ishtar\View\View;
use Theme\Divider\DividerRepository;

/**
 * Class Divider
 * @package Theme\Block\Divider
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $name
 * @property View $View
 */
final class Divider extends Block
{
    protected $name = 'divider';
    protected $manifest_path = WP_CONTENT_DIR . '/themes/vincent-ragosta';

    public function render($attributes, $content, $block)
    {
        $View = $this->getView();
        return new $View();
    }
}
