<?php

namespace Theme\Controller;

use Ishtar\Utility\ElementUtility;
use Theme\Project\Project;

/**
 * Class BarMenuController
 * @package Theme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BarMenuController
{
    public function __construct()
    {
        add_action('wp_footer', function() {
            if (!is_singular(Project::POST_TYPE)) {
                echo ElementUtility::create('div', '', [
                    'class' => 'bar-menu animations',
                    'id' => 'bar-menu'
                ]);
            }
        });
    }
}
