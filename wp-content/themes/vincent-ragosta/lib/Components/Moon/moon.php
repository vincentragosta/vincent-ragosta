<?php
/**
 * Expected:
 * @var Image $image
 * @var string $title
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Gilgamesh\Image;
use Ishtar\Utility\ComponentUtility;
?>

<div <?= ComponentUtility::attributes('moon', $class_modifiers, $element_attributes); ?>>
    <div class="moon__layer">
        <div class="moon__layer">
            <div class="moon__layer">
                <div class="moon__layer">
                    <div class="moon__layer">
                        <div class="moon__layer">
                            <div class="moon__center" <?= $image instanceof Image ? 'style="background-image:url(' . $image->url . ');"' : ''; ?>></div>
                            <?php if ($title): ?>
                                <h2 class="moon__title typography--large text--uppercase"><?= $title; ?></h2>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
