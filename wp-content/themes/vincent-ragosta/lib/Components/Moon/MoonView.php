<?php

namespace Theme\Components\Moon;

use Gilgamesh\Image;
use Ishtar\View\ComponentView;

/**
 * Class MoonView
 * @package Theme\Components\Moon
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 * @property Image|string $image
 * @property string $title
 */
class MoonView extends ComponentView
{
    const IMAGE_SIZE = 750;

    protected $name = 'moon';
    protected static $default_properties = [
        'image' => null,
        'title' => ''
    ];

    public function __construct($image = '', string $title = '')
    {
        parent::__construct(compact('image', 'title'));
        if ($this->image instanceof Image) {
            $this->image = $this->image->width(static::IMAGE_SIZE)->height(static::IMAGE_SIZE);
        }
    }
}
