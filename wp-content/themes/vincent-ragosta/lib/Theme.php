<?php

namespace Theme;

use Gilgamesh\Theme as ThemeBase;

/**
 * Class Theme
 * @package Theme
 * @author  Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Theme extends ThemeBase
{
    const EXTENSIONS = [
        Options\ThemeSettings::class,
        Controller\BarMenuController::class,
        PokemonCard\PokemonCardController::class,
        PokemonCard\PokemonCardRestController::class,
        Project\ProjectController::class,
        Block\PokemonCard\PokemonCard::class,
        Block\PokemonCardFilters\PokemonCardFilters::class,
        Block\Divider\Divider::class,
        Block\ProjectCard\ProjectCard::class,
        Block\TitleCard\TitleCard::class,
        Block\Hero\Hero::class
    ];

    public function __construct()
    {
        parent::__construct();
        add_filter('big_image_size_threshold', '__return_false');
//        add_filter('big_image_size_threshold', function ($threshold) {
//            return 1000000; // new threshold
//        }, 100, 1);
    }

    public function setup()
    {
        parent::setup();
    }

    public function assets()
    {
        parent::assets();
    }

    public function blockEditorAssets()
    {
        parent::blockEditorAssets();
    }
}
