<?php

namespace Theme\Project;

use Gilgamesh\Post\PostRepository;

/**
 * Class ProjectRepository
 * @package Theme\Project
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ProjectRepository extends PostRepository
{
    protected $model_class = Project::class;
}
