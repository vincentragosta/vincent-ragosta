<?php

namespace Theme\Project;

/**
 * Class ProjectController
 * @package Theme\Project
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ProjectController
{
    public function __construct()
    {
        add_filter('use_block_editor_for_post_type', [$this, 'disableGutenberg'], 10, 2);
        add_action($this->getFieldManagerHook(), [$this, 'addFields']);
    }

    public function disableGutenberg($current_status, $post_type)
    {
        if ($post_type === Project::POST_TYPE) {
            $current_status = false;
        }
        return $current_status;
    }

    public function addFields()
    {
        try {
            $fm = new \Fieldmanager_Group('Project', [
                'name' => 'properties',
                'children' => [
                    'website_url' => new \Fieldmanager_TextField('Website Url'),
                    'date_range' => new \Fieldmanager_TextField('Date Range'),
                    'concept' => new \Fieldmanager_TextArea('Concept'),
                    'development' => new \Fieldmanager_TextArea('Development'),
                    'logo' => new \Fieldmanager_Media('Logo'),
                    'full_page_image' => new \Fieldmanager_Media('Full Page Image'),
                    'alt_full_page_image' => new \Fieldmanager_Media('Full Page Image (Alt)')
                ]
            ]);
            $fm->add_meta_box('Project Fields', [Project::POST_TYPE]);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    protected function getFieldManagerHook()
    {
        return sprintf('fm_post_%s', Project::POST_TYPE);
    }
}
