<?php

namespace Theme\Project;

use Gilgamesh\Image;
use Gilgamesh\Post\Post;

/**
 * Class Project
 * @package Theme\Project
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $website_url
 * @property string $date_range
 * @property string $concept
 * @property string $development
 * @property Image|bool $logo
 * @property Image|bool $full_page_image
 * @property Image|bool $alt_full_page_image
 * @property \WP_Term $project_type
 *
 * @property array $properties
 */
class Project extends Post
{
    const POST_TYPE = 'project';
    const TAXONOMY = 'project-type';

    protected function getWebsiteUrl()
    {
        $properties = $this->properties;
        return $properties['website_url'] ?: '';
    }

    protected function getDateRange()
    {
        $properties = $this->properties;
        return $properties['date_range'] ?: '';
    }

    protected function getConcept()
    {
        $properties = $this->properties;
        return $properties['concept'] ?: '';
    }

    protected function getDevelopment()
    {
        $properties = $this->properties;
        return $properties['development'] ?: '';
    }

    protected function getLogo()
    {
        $properties = $this->properties;
        return $properties['logo'] ? Image::get_by_attachment_id($properties['logo']) : false;
    }

    protected function getFullPageImage()
    {
        $properties = $this->properties;
        return $properties['full_page_image'] ? Image::get_by_attachment_id($properties['full_page_image']) : false;
    }

    protected function getAltFullPageImage()
    {
        $properties = $this->properties;
        return $properties['alt_full_page_image'] ? Image::get_by_attachment_id($properties['alt_full_page_image']) : false;
    }

    protected function getProjectType()
    {
        if (empty($terms = $this->terms(Project::TAXONOMY))) {
            return null;
        }
        return $terms[0];
    }

    public function getProjectTypeName()
    {
        $ProjectType = $this->project_type;
        if (!$ProjectType instanceof \WP_Term) {
            return '';
        }
        return $ProjectType->name;
    }
}
