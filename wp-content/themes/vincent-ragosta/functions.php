<?php

use Theme\Theme;

global $Theme;

if (empty($Theme)) {
    $Theme = new Theme();
}
