<?php use Gilgamesh\Utility\NavMenuUtility; ?>

<p id="scrolldown-bar" class="scrolldown-bar text--uppercase">Scrolldown</p>
<div id="curtain" class="curtain"></div>
<?php if (!empty($nav_items = NavMenuUtility::getPrimaryNavigationItems())): ?>
    <div class="primary-navigation" id="primary-navigation">
        <ul class="primary-navigation__list text--centered text--uppercase">
            <?php foreach($nav_items as $NavItem): ?>
                <li>
                    <!-- TODO: Figure out a way to remove the heading here -->
                    <h2 class="typography--xlarge text--weightless">
                        <a href="<?= $NavItem->url; ?>"><?= $NavItem->title; ?></a>
                    </h2>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
