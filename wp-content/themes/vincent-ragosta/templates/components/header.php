<?php

use Gilgamesh\Social\SocialIcon;
use Gilgamesh\Utility\AssetUtility;
use Ishtar\Components\Icon\IconView;
use Theme\Options\ThemeSettings;

?>

<header id="header-nav" class="header-nav">
    <h1 class="header-nav__heading heading heading--default text--weightless"><a href="<?= home_url(); ?>"><?= get_bloginfo('name'); ?></a></h1>
    <div class="header-nav__details">
        <?php if (!($SocialIconsCollection = ThemeSettings::socialIcons())->isEmpty()): ?>
            <ul class="header-nav__list list list--inline">
                <?php foreach($SocialIconsCollection->getAll() as $SocialIcon): /** @var SocialIcon $SocialIcon */ ?>
                    <li>
                        <a href="<?= $SocialIcon->getUrl(); ?>" target="_blank">
                            <?= new IconView($SocialIcon->getIconName()); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
                <li class="typography--large has-sub-menu">Pokemon
                    <ul class="header-nav__sub-menu list list--unstyled">
                        <li><a href="<?= home_url('/pokemon/'); ?>">Collection</a></li>
                        <li><a href="<?= home_url('/pokemon/rip-and-ship'); ?>">Rip & Ship</a></li>
                        <li><a href="<?= home_url('/pokemon/giveaway'); ?>">Giveaway</a></li>
                    </ul>
                </li>
            </ul>
        <?php endif; ?>
        <!-- TODO: Consider making this a react component? -->
        <button type="button" id="hamburger" class="button header-nav__toggle hamburger"></button>
    </div>
</header>
