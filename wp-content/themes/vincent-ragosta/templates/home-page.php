<?php
/**
 * Template Name: Test Home Page
 */

use Theme\PokemonCard\PokemonCard;
use Theme\PokemonCard\PokemonCardRepository;

//var_dump(get_post_meta('185156', 'price', true));
//var_dump(get_post_meta('185156', 'image_url', true));
//var_dump((new PokemonCardRepository())->findByPriceSetAndImageUrl(get_post_meta('185156', 'price', true), 'team-rocket', get_post_meta('185156', 'image_url', true)));
?>

<!--<div class="container">-->
<!--    <div class="wp-block-columns">-->
<!--        <div class="wp-block-column">-->
<!--            <div id="pokemon-filters" class="pokemon-filters">-->
<!--                <div class="pokemon-filters__filters">-->
<!--                    <div class="pokemon-filters__filter">-->
<!--                        <label for="pokemon-grade">Grade</label>-->
<!--                        <select id="pokemon-grade" class="pokemon-filters__select">-->
<!--                            <option value="">Select Grade</option>-->
<!--                            --><?php //foreach(get_terms(['taxonomy' => PokemonCard::GRADE_TAXONOMY]) as $Grade): ?>
<!--                                <option value="--><?//= $Grade->term_id; ?><!--">--><?//= $Grade->name; ?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!--                        </select>-->
<!--                    </div>-->
<!--                    <div class="pokemon-filters__filter">-->
<!--                        <label for="pokemon-set">Set</label>-->
<!--                        <select id="pokemon-set" class="pokemon-filters__select">-->
<!--                            <option value="">Select Set</option>-->
<!--                            --><?php //foreach(get_terms(['taxonomy' => PokemonCard::SET_TAXONOMY]) as $Set): ?>
<!--                                <option value="--><?//= $Set->term_id; ?><!--">--><?//= $Set->name; ?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="pokemon-filters__cards"></div>-->
<!--                <div class="pokemon-filters__pagination wp-block-buttons"></div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
