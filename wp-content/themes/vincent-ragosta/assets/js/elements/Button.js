import React from 'react';

/**
 * TODO: add ability to add data attributes
 * class Button
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Button extends React.Component {
    onButtonClick(event) {
        this.props.onClick(event);
    }
    render() {
        // TODO: change to array notation
        let classes = 'wp-block-button';
        if (this.props.className) {
            classes += ' ' + this.props.className;
        }
        return (
            <div className={classes}>
                <a className={'wp-block-button__link'} onClick={this.onButtonClick.bind(this)}>
                    {this.props.text}
                </a>
            </div>
        );
    }
}

export default Button;
