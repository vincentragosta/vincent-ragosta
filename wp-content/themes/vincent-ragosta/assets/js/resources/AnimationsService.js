import Viewport from "@vincentragosta/ishtar/assets/js/resources/Viewport";

/**
 * TODO: Break this down to individual service classes
 *
 * class AnimationsService
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AnimationsService {
    constructor() {
        this.primaryNavigation();
        this.scrolldownBar();
        this.curtain();
        this.hero();
        this.titleCards();
        this.fadeAnimations();
    }

    primaryNavigation() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let primaryNavigation = document.getElementById('primary-navigation');
            if (primaryNavigation) {
                this.setAnimatedStyle(primaryNavigation, 'transition: 0.4s cubic-bezier(0.785, 0.135, 0.15, 0.86);');
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    scrolldownBar() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let scrolldownBar = document.getElementById('scrolldown-bar');
            let INACTIVE_CLASS = 'scrolldown-bar--hide';
            if (scrolldownBar) {
                scrolldownBar.classList.toggle(INACTIVE_CLASS, window.scrollY !== 0);
                document.addEventListener('scroll', () => {
                    scrolldownBar.classList.toggle(INACTIVE_CLASS, window.scrollY !== 0);
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    curtain() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let curtain = document.getElementById('curtain');
            if (curtain) {
                this.setAnimatedStyle(curtain, 'transform: translate3d(0, -100%, 0);');
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    hero() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let heroEl = document.getElementById('hero');
            if (heroEl) {
                let title = heroEl.querySelector('.hero__title');
                let moon = heroEl.querySelector('.moon');
                this.setAnimatedState(title, heroEl.querySelector('.hero__divider'), heroEl.querySelector('.hero__subtext'));
                if (moon) {
                    if (Viewport.isInViewport(title)) {
                        this.setAnimatedStyle(moon, 'transform: translateX(0%) translateZ(0px);', 50);
                    } else {
                        this.removeAnimatedStyle(moon, 50);
                    }
                }
                window.addEventListener('scroll', () => {
                    this.setAnimatedState(heroEl.querySelector('.hero__title'), heroEl.querySelector('.hero__divider'), heroEl.querySelector('.hero__subtext'));
                    if (moon) {
                        if (Viewport.isInViewport(title)) {
                            this.setAnimatedStyle(moon, 'transform: translateX(0%) translateZ(0px);', 50);
                        } else {
                            this.removeAnimatedStyle(moon, 50);
                        }
                    }
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    fadeAnimations() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let FADE_ANIMATION = 'animations--fade';
            let fadeAnimations = document.getElementsByClassName(FADE_ANIMATION);
            if (fadeAnimations) {
                [...fadeAnimations].forEach((value) => {
                    value.classList.toggle(FADE_ANIMATION, !Viewport.isInViewport(value.querySelector('h2.typography--large')));
                    window.addEventListener('scroll', () => {
                        value.classList.toggle(FADE_ANIMATION, !Viewport.isInViewport(value.querySelector('h2.typography--large')));
                    });
                    blockLoaded = true;
                });
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    titleCards() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let titleCards = document.getElementsByClassName('title-card');
            if (titleCards) {
                [...titleCards].forEach((element) => {
                    this.titleCardAnimation(element);
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }

    titleCardAnimation(element) {
        let title = element.querySelector('.title-card__title');
        let divider = element.querySelector('.title-card__divider');
        let subtext = element.querySelector('.title-card__subtext');
        let image = element.querySelector('.title-card__image');

        if (Viewport.isInViewport(title)) {
            if (!element.classList.contains('in-view')) {
                element.classList.add('in-view');
            }
        } else {
            if (element.classList.contains('in-view')) {
                element.classList.remove('in-view');
            }
        }
        this.setAnimatedState(title, divider, subtext, image);
        window.addEventListener('scroll', () => {
            if (Viewport.isInViewport(title)) {
                if (!element.classList.contains('in-view')) {
                    element.classList.add('in-view');
                }
            } else {
                if (element.classList.contains('in-view')) {
                    element.classList.remove('in-view');
                }
            }
            this.setAnimatedState(title, divider, subtext, image);
        });
    }

    setAnimatedState(title, divider, subtext, image = null) {
        if (Viewport.isInViewport(title)) {
            this.setAnimatedStyle(title, 'transform: translateX(0%) translateZ(0px);');
            this.setAnimatedStyle(divider, 'transform: translateX(0%) translateZ(0px);', 50);
            this.setAnimatedStyle(subtext, 'transform: translateX(0%) translateZ(0px);', 35);
            if (image) {
                this.setAnimatedStyle(image, 'transform: scale(1);', 50);
            }
        } else {
            this.removeAnimatedStyle(title);
            this.removeAnimatedStyle(divider, 50);
            this.removeAnimatedStyle(subtext, 35);
            if (image) {
                this.removeAnimatedStyle(image, 50);
            }
        }
    }

    setAnimatedStyle(element, value, duration = 0) {
        if (!element.hasAttribute('style')) {
            if (!duration) {
                element.setAttribute('style', value);
            } else {
                setTimeout(() => {
                    element.setAttribute('style', value);
                }, duration);
            }
        }
    }

    removeAnimatedStyle(element, duration) {
        if (element.hasAttribute('style')) {
            if (!duration) {
                element.removeAttribute('style');
            } else {
                setTimeout(() => {
                    element.removeAttribute('style');
                }, duration);
            }
        }
    }
}

new AnimationsService();
