import ReactDOM from 'react-dom';
import PokemonCardFilters from '../components/PokemonCardFilters';

/**
 * class PokemonCardFiltersService
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardFiltersService {
    constructor() {
        this.id = 'pokemon-card-filters';
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let el = document.getElementById(this.id);
            if (el) {
                ReactDOM.render(<PokemonCardFilters/>, el.querySelector('.pokemon-card-filters__filters-container'));
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

new PokemonCardFiltersService();
