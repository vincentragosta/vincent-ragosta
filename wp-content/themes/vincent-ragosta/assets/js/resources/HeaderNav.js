/**
 * TODO: Move to Ishtar
 *
 * class HeaderNav
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class HeaderNav {
    constructor() {
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let headerNav = document.getElementById('header-nav');
            if (headerNav) {
                headerNav.classList.toggle('header-nav--scrolling', window.scrollY !== 0);
                document.addEventListener('scroll', () => {
                    headerNav.classList.toggle('header-nav--scrolling', window.scrollY !== 0);
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

new HeaderNav();
