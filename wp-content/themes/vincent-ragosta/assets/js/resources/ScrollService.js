/**
 * class ScrollService
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ScrollService {
    constructor() {
        this.hero = document.getElementById('hero');
        this.projects = document.getElementById('projects');

        if (this.hero && this.projects) {
            // this.hero.addEventListener('scroll', (e) => {
            //     e.preventDefault();
            // console.log('scrolled');
            // if (this.isInViewport(this.projects)) {
            //     console.log('here');
            //     this.doScrolling('#projects', 1000);
            // }
            // window.scrollTo({
            //     top: this.projects.offsetTop,
            //     behavior: "smooth"
            // });
            // scroll({
            //     top: this.projects.offsetTop,
            //     behavior: "smooth"
            // });
            // });
            // setTimeout(() => {
            window.addEventListener('scroll', this.doScrolling('#projects', 1000));
            // }, 1000);
        }
    }

    getElementY(query) {
        return window.pageYOffset + document.querySelector(query).getBoundingClientRect().top
    }

    doScrolling(element, duration) {
        let startingY = window.pageYOffset;
        let elementY = this.getElementY(element);
        // If element is close to page's bottom then window will scroll only to some position above the element.
        let targetY = document.body.scrollHeight - elementY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elementY;
        let diff = targetY - startingY;

        // Easing function: easeInOutCubic
        // From: https://gist.github.com/gre/1650294
        let easing = function (t) {
            return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
        };
        let start;

        if (!diff) return;

        // Bootstrap our animation - it will get called right before next frame shall be rendered.
        window.requestAnimationFrame(function step(timestamp) {
            if (!start) start = timestamp;
            // Elapsed miliseconds since start of scrolling.
            let time = timestamp - start;
            // Get percent of completion in range [0, 1].
            let percent = Math.min(time / duration, 1);
            // Apply the easing.
            // It can cause bad-looking slow frames in browser performance tool, so be careful.
            percent = easing(percent);
            window.scrollTo(0, startingY + diff * percent);

            // Proceed with animation as long as we wanted it to.
            if (time < duration) {
                window.requestAnimationFrame(step);
            }
        });
    }

    isInViewport(element) {

        const {top, bottom} = element.getBoundingClientRect();
        const vHeight = (window.innerHeight || document.documentElement.clientHeight);

        return (
            (top > 0 || bottom > 0) &&
            top < vHeight
        );

        // let rect = element.getBoundingClientRect();
        // let html = document.documentElement;
        // return (
        //     rect.top >= 0 &&
        //     rect.left >= 0 &&
        //     rect.bottom <= (window.innerHeight || html.clientHeight) &&
        //     rect.right <= (window.innerWidth || html.clientWidth)
        // );
    }
}

// export default ScrollService;
// new ScrollService();

// document.onreadystatechange = () => {
    // if (document.readyState === ("complete")) {
    //     setTimeout(function () {
    //         new ScrollService();
    //     }, 100);
    // }
// };

// window.onload = (event) => {
//     new ScrollService();
// };
