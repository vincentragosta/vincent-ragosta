/**
 * class Hamburger
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class Hamburger {
    constructor() {
        this.activeClass = 'hamburger--active';
        this.headerNavScrolling = 'header-nav--scrolling';
        this.fadeAnimtion = 'animations--fade';

        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let hamburger = document.getElementById('hamburger');
            if (hamburger) {
                let headerNav = document.getElementById('header-nav');
                let barMenu = document.getElementById('bar-menu');
                hamburger.addEventListener('click', () => {
                    if (headerNav.classList.contains(this.headerNavScrolling)) {
                        headerNav.classList.remove(this.headerNavScrolling);
                    }
                    barMenu.classList.toggle(this.fadeAnimtion, !barMenu.classList.contains(this.fadeAnimtion));
                });
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

/** TODO: consider making an extension service class so you can always export default here **/
new Hamburger();
