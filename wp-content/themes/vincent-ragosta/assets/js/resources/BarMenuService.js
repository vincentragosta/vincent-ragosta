import ReactDOM from 'react-dom';
import BarMenu from '../components/BarMenu';

/**
 * class BarMenuService
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BarMenuService {
    constructor() {
        this.id = 'bar-menu';
        let blockLoaded = false;
        let blockLoadedInterval = setInterval(() => {
            let el = document.getElementById(this.id);
            if (el) {
                ReactDOM.render(<BarMenu/>, el);
                blockLoaded = true;
            }
            if (blockLoaded) {
                clearInterval(blockLoadedInterval);
            }
        }, 500);
    }
}

new BarMenuService();
