// CSS
import '../main.scss';

// JS
import './utility/asset-importer';
import './resources/PokemonCardFiltersService';
import './resources/AnimationsService';
import './resources/BarMenuService';
import './resources/HeaderNav';
import './resources/Hamburger';
