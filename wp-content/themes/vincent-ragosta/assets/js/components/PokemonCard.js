import React from 'react';

/**
 * class PokemonCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCard extends React.Component {
    render() {
        return (
            <div className="pokemon-card">
                <div className={"pokemon-card__image-container"}>
                    <img src={this.props.imageUrl} width={'200px'} height={'277px'}/>
                </div>
            </div>
        );
    }
}

export default PokemonCard;
