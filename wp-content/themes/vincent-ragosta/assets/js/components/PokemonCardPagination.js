import React from 'react';
import Button from "../elements/Button";

/**
 * class PokemonCardPagination
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardPagination extends React.Component {
    onButtonClick(event) {
        const parentClasses = event.target.parentNode.classList;
        this.props.onClick(this.getPreviousOrNext(parentClasses));
    }

    // TODO: Perhaps setup a state value instead? Add data attribute to buttons instead of class?
    getPreviousOrNext(classes) {
        let prevOrNext = '';
        classes.forEach((value) => {
            if (value.includes('previous')) {
                prevOrNext = 'previous';
            }
            if (value.includes('next')) {
                prevOrNext = 'next';
            }
        });
        return prevOrNext;
    }

    render() {
        return (
            <div className="pokemon-card-filters__pagination wp-block-buttons">
                {this.props.currentPage !== 1 &&
                <Button text={"Previous"} className={'pokemon-card-filters__pagination-previous'}
                        onClick={this.onButtonClick.bind(this)}/>}
                {this.props.currentPage < this.props.totalPages &&
                <Button text={"Next"} className={'pokemon-card-filters__pagination-next'}
                        onClick={this.onButtonClick.bind(this)}/>}
            </div>
        );
    }
}

export default PokemonCardPagination;
