import React from 'react';

/**
 * class BarMenuListItem
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BarMenuListItem extends React.Component {
    constructor(props) {
        super(props);
        this.props.className = 'bar-menu__list-item';
    }

    onClick(event) {
        this.props.onClick(event.target.parentNode.getAttribute('data-section-id'));
    }

    render() {
        return (<li data-section-id={this.props.id} onClick={this.onClick.bind(this)} className={this.props.className}><hr className="bar-menu__line"/></li>);
    }
}

export default BarMenuListItem;
