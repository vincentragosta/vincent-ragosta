import apiFetch from '@wordpress/api-fetch';
import React from 'react';
import PokemonCard from "./PokemonCard";
import PokemonCardFilter from "./PokemonCardFilter";
import PokemonCardPagination from "./PokemonCardPagination";
import PokemonCardSearch from "./PokemonCardSearch";

const {addQueryArgs} = wp.url;

/**
 * class PokemonCardFilters
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardFilters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            grade: '',
            set: '',
            s: ''
        };

        this.props.page = 1;
        this.initialize();
    }

    initialize() {
        apiFetch({
            path: this.getTaxonomyEndpoint('grade')
        }).then((grades) => {
            this.props.grades = grades;
            // TODO: check if overriding shouldComponentUpdate() would prevent us from having to use force update here
            this.forceUpdate();
        });

        apiFetch({
            path: this.getTaxonomyEndpoint('set')
        }).then((sets) => {
            this.props.sets = sets;
            this.forceUpdate();
        });

        apiFetch({
            method: 'post',
            path: this.getPokemonCardEndpoint(),
            body: JSON.stringify(this.getDefaultPokemonCardQuery()),
            headers: {
                'Content-Type': 'application/json'
            },
            parse: false
        }).then((response) => {
            this.props.totalPages = response.headers.get('X-WP-TotalPages');
            this.props.totalCards = response.headers.get('X-WP-Total');
            response.json().then((res) => {
                this.props.cards = res;
                this.forceUpdate();
            });
        });
    }

    onFilterChange(field, value) {
        this.props.page = 1;
        let opts = this.getDefaultPokemonCardQuery();
        if (field === 'grade') {
            this.props.grade = value;
            if (value) {
                opts.grade = value;
            }
            if (this.props.set) {
                opts.set = this.props.set;
            }
        }
        if (field === 'set') {
            this.props.set = value;
            if (value) {
                opts.set = value;
            }
            if (this.props.grade) {
                opts.grade = this.props.grade;
            }
        }
        if (this.state.s) {
            opts.s = this.state.s;
        }
        apiFetch({
            method: 'post',
            path: this.getPokemonCardEndpoint(),
            body: JSON.stringify(opts),
            headers: {
                'Content-Type': 'application/json'
            },
            parse: false
        }).then((response) => {
            this.props.totalPages = response.headers.get('X-WP-TotalPages');
            this.props.totalCards = response.headers.get('X-WP-Total');
            response.json().then((res) => {
                this.props.cards = res;
                this.setState({[field]: value});
            });
        });
    }

    onSearchChange(field, value) {
        this.props.page = 1;
        let opts = this.getDefaultPokemonCardQuery();
        if (this.props.set) {
            opts.set = this.props.set;
        }
        if (this.props.grade) {
            opts.grade = this.props.grade;
        }
        opts.s = value;
        apiFetch({
            method: 'post',
            path: this.getPokemonCardEndpoint(),
            body: JSON.stringify(opts),
            headers: {
                'Content-Type': 'application/json'
            },
            parse: false
        }).then((response) => {
            this.props.totalPages = response.headers.get('X-WP-TotalPages');
            this.props.totalCards = response.headers.get('X-WP-Total');
            response.json().then((res) => {
                this.props.cards = res;
                this.setState({[field]: value});
            });
        });
    }

    onPaginationButtonClick(prevOrNext) {
        if (prevOrNext === 'next') {
            this.props.page = this.props.page + 1;
        } else if (prevOrNext === 'previous') {
            this.props.page = this.props.page - 1;
        }

        let opts = this.getDefaultPokemonCardQuery();
        if (this.state.grade) {
            opts.grade = this.state.grade;
        }
        if (this.state.set) {
            opts.set = this.state.set;
        }
        if (this.state.s) {
            opts.s = this.state.s;
        }
        apiFetch({
            method: 'post',
            path: this.getPokemonCardEndpoint(),
            body: JSON.stringify(opts),
            headers: {
                'Content-Type': 'application/json'
            },
            parse: false
        }).then((response) => {
            this.props.totalPages = response.headers.get('X-WP-TotalPages');
            this.props.totalCards = response.headers.get('X-WP-Total');
            response.json().then((res) => {
                this.props.cards = res;
                this.forceUpdate();
            });
        });
    }

    getTaxonomyEndpoint(taxonomy) {
        return addQueryArgs('/wp/v2/' + taxonomy, {per_page: -1});
    }

    getDefaultPokemonCardQuery() {
        return {
            page: this.props.page,
            meta_key: 'price',
            orderby: 'meta_value_num'
        };
    }

    getPokemonCardEndpoint() {
        return '/pokemon-cards/find';
    }

    render() {
        return (
            <div className={"pokemon-card-filters__filters-inner"}>
                <div className={"pokemon-card-filters__filters"}>
                    {this.props.grades &&
                    <PokemonCardFilter filterId={'pokemon-grade'} label={'Grade'} name={'grade'}
                                       terms={this.props.grades}
                                       onChange={this.onFilterChange.bind(this)}/>}
                    {this.props.sets &&
                    <PokemonCardFilter filterId={'pokemon-set'} label={'Set'} name={'set'} terms={this.props.sets}
                                       onChange={this.onFilterChange.bind(this)}/>}
                    <PokemonCardSearch filterid={'pokemon-search'} label={'Search'} name={'s'}
                                       onChange={this.onSearchChange.bind(this)}/>
                </div>
                {this.props.cards && this.props.cards.length > 0 &&
                <div className="pokemon-card-filters__cards">
                    {this.props.cards.map((card) => (
                        <PokemonCard imageUrl={card.image_url}/>
                    ))}
                </div>
                }
                {this.props.cards && this.props.cards.length == 0 &&
                <p className="pokemon-card-filters__no-cards">There are no cards with this selection.</p>}
                {this.props.totalPages && this.props.cards && this.props.cards.length >= 24 && this.props.page <= this.props.totalPages &&
                <PokemonCardPagination currentPage={this.props.page} totalPages={this.props.totalPages}
                                       onClick={this.onPaginationButtonClick.bind(this)}/>
                }
            </div>
        );
    }
}

export default PokemonCardFilters;
