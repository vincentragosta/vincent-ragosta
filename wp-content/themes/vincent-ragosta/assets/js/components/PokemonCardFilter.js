import React from 'react';

/**
 * class PokemonCardFilter
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardFilter extends React.Component {
    onFieldChange(event) {
        this.props.onChange(event.target.name, event.target.value);
    }

    render() {
        return (
            <div className="pokemon-card-filters__filter">
                <label htmlFor="pokemon-set">{this.props.label}</label>
                <select id={this.props.filterId} className="pokemon-card-filters__select" name={this.props.name}
                        onChange={this.onFieldChange.bind(this)}>
                    <option value="">Select {this.props.label}</option>
                    {this.props.terms.map((term, index) => (
                        <option value={term.id} key={index}>{term.name}</option>
                    ))}
                </select>
            </div>
        );
    }
}

export default PokemonCardFilter;
