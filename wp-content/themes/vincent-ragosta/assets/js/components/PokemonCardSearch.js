import React from 'react';

/**
 * class PokemonCardFilter
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PokemonCardSearch extends React.Component {
    onFieldChange(event) {
        this.props.onChange(event.target.name, event.target.value);
    }

    render() {
        return (
            <div className="pokemon-card-filters__filter">
                <label htmlFor="pokemon-set">{this.props.label}</label>
                <input id={this.props.filterId} className="pokemon-card-filters__input" name={this.props.name} placeholder={'Search for card...'}
                       onChange={this.onFieldChange.bind(this)}/>
            </div>
        );
    }
}

export default PokemonCardSearch;
