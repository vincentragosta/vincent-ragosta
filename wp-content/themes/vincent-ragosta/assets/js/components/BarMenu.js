import React from 'react';
import Viewport from "@vincentragosta/ishtar/assets/js/resources/Viewport";
import BarMenuListItem from "./BarMenuListItem";

/**
 * class BarMenu
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class BarMenu extends React.Component {
    constructor(props) {
        super(props);
        this.props.className = 'bar-menu__list-item';
        // change [id] for an exclusion class
        this.props.sections = document.querySelectorAll('.container-fluid > .wp-block-columns[id]');
        this.activeClass = 'bar-menu__list-item--active';
    }

    componentDidMount() {
        if (this.props.sections) {
            let barMenu = document.getElementById('bar-menu');
            if (!barMenu) {
                return;
            }
            let listItems = barMenu.querySelectorAll('.bar-menu__list-item');
            [...this.props.sections].map((section, index) => {
                let listItem = barMenu.querySelector('[data-section-id=' + section.id + ']');
                this.addListItemActiveClassBySection(listItem, section, listItems);
                window.addEventListener('scroll', this.debounce(() => {
                    this.addListItemActiveClassBySection(listItem, section, listItems);
                }, 100));
            });
        }
    }

    addListItemActiveClassBySection(listItem, section, listItems) {
        if (Viewport.isInViewport(section)) {
            if (!listItem.classList.contains(this.activeClass)) {
                [...listItems].map((item, index) => {
                    return item.classList.remove(this.activeClass);
                });
                listItem.classList.add(this.activeClass);
            }
        }
    }

    debounce(callback, wait) {
        let timeout;
        return (...args) => {
            const context = this;
            clearTimeout(timeout);
            timeout = setTimeout(() => callback.apply(context, args), wait);
        };
    }

    onClick(id) {
        let section = document.getElementById(id);
        if (section) {
            section.scrollIntoView({behavior: 'smooth'});
        }
    }

    render() {
        return (
            <ul className="bar-menu__list">
                {[...this.props.sections].map((section, index) => {
                    return (<BarMenuListItem id={section.id} onClick={this.onClick.bind(this)}
                                             key={index}>{section}</BarMenuListItem>);
                })}
            </ul>
        );
    }
}

export default BarMenu;
