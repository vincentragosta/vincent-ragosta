import manifest from '../../../manifest/pokemon-card-filters';
import BlockFactory from '@vincentragosta/ereshkigal/assets/js/utility/BlockFactory';

const {__} = wp.i18n;
const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;
const {InspectorControls, useBlockProps} = wp.blockEditor;
const {PanelBody, TextareaControl} = wp.components;

const PokemonCardFilters = BlockFactory.create(manifest, {
    edit: ((props) => {
        const {attributes, setAttributes} = props;
        const blockProps = useBlockProps();
        let {text} = attributes;
        return (
            <div{...blockProps}>
                <ServerSideRender block='vincentragosta/pokemon-card-filters' attributes={attributes} />
                <InspectorControls>
                    <PanelBody title={__('Attributes', 'vincentragosta')} key={'attributes'}>
                        <TextareaControl
                            label={"Text"}
                            value={text}
                            onChange={(text) => {
                                setAttributes({text});
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </div>
        );
    })
});

export default PokemonCardFilters;
