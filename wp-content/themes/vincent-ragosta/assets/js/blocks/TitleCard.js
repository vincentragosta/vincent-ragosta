// TODO: Create @ for manifest
import manifest from '../../../manifest/title-card';
import {withSelect} from '@wordpress/data';
import BlockFactory from '@vincentragosta/ereshkigal/assets/js/utility/BlockFactory';

const {__} = wp.i18n;
const {PanelBody, TextControl, TextareaControl, Button, ResponsiveWrapper, Spinner} = wp.components;
const {InspectorControls, useBlockProps, MediaUpload} = wp.blockEditor;
const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;

const TitleCard = BlockFactory.create(manifest, {
    // edit: ((props) => {
    edit: withSelect((select, props) => {
        const {getMedia} = select('core');
        const {imageId} = props.attributes;
        return {
            image: imageId ? getMedia(imageId) : null
        };
    })((props) => {
        const {setAttributes, attributes, image} = props;
        const blockProps = useBlockProps();
        let {title, subtext, imageId} = attributes;

        return (
            <div {...blockProps}>
                <ServerSideRender block='vincentragosta/title-card' attributes={attributes}/>
                <InspectorControls>
                    <PanelBody title={__('Attributes', 'vincentragosta')} key={'attributes'}>
                        <TextControl
                            label={'Title'}
                            value={title}
                            onChange={(title) => {
                                setAttributes({title});
                            }}
                        />
                        <TextareaControl
                            label={"Subtext"}
                            value={subtext}
                            onChange={(subtext) => {
                                setAttributes({subtext});
                            }}
                        />
                        <MediaUpload
                            title={__('Image', 'vincentragosta')}
                            onSelect={(media) => {
                                // console.log(media);
                                setAttributes({
                                    imageId: media.id
                                });
                            }}
                            allowedTypes={['image']}
                            value={imageId}
                            render={({open}) => (
                                <Button onClick={open}>
                                    Set Image
                                    {/*{!image && (__('Set Image', 'vincentragosta'))}*/}
                                    {/*/!*{!!imageId && !image && <Spinner/>}*!/*/}
                                    {/*{image &&*/}
                                    {/*<ResponsiveWrapper*/}
                                    {/*    naturalWidth={image.media_details.width}*/}
                                    {/*    naturalHeight={image.media_details.height}>*/}
                                    {/*    <img src={image.source_url}*/}
                                    {/*         alt={__('Image', 'vincentragosta')}*/}
                                    {/*    />*/}
                                    {/*</ResponsiveWrapper>*/}
                                    {/* }*/}
                                </Button>
                            )}
                        />
                    </PanelBody>
                </InspectorControls>
            </div>
        )
    })
});

export default TitleCard;
