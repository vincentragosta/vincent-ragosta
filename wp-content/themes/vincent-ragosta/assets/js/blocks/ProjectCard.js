import manifest from '../../../manifest/project-card';
import PostPicker from '@vincentragosta/ereshkigal/assets/js/components/PostPicker';
import BlockFactory from '@vincentragosta/ereshkigal/assets/js/utility/BlockFactory';

const {useBlockProps} = wp.blockEditor;

const { __ } = wp.i18n;
const {PanelBody} = wp.components;
const {InspectorControls} = wp.blockEditor;
const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;

const ProjectCard = BlockFactory.create(manifest, {
    edit: ((props) => {
        const {setAttributes, attributes} = props;
        const blockProps = useBlockProps();

        let {picks} = attributes;
        picks = picks === '' || typeof picks === 'undefined' ? [] : picks.split(',');

        return (
            <div {...blockProps}>
                <ServerSideRender block='vincentragosta/project-card' attributes={attributes} />
                <InspectorControls>
                    <PanelBody title={__('Selected Projects', 'vincentragosta')} key={'posts'}>
                        <PostPicker
                            isMulti={false}
                            initialSelection={picks}
                            postTypes={['project']}
                            onChange={(picks) => {
                                setAttributes({picks});
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </div>
        )
    })
});

export default ProjectCard;
