// TODO: use @vincentragosta for manifest
import manifest from '../../../manifest/divider';
import BlockFactory from '@vincentragosta/ereshkigal/assets/js/utility/BlockFactory';

const {useBlockProps} = wp.blockEditor;

const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;

const Divider = BlockFactory.create(manifest, {
    edit: ((props) => {
        const {attributes} = props;
        const blockProps = useBlockProps();

        return (
            <div {...blockProps}>
                <ServerSideRender block='vincentragosta/divider' attributes={attributes} />
            </div>
        )
    })
});

export default Divider;
