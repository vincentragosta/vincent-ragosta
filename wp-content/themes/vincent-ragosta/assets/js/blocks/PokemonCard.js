import manifest from '../../../manifest/pokemon-card';
import PostPicker from '@vincentragosta/ereshkigal/assets/js/components/PostPicker';
import BlockFactory from '@vincentragosta/ereshkigal/assets/js/utility/BlockFactory';

const { __ } = wp.i18n;
const {PanelBody} = wp.components;
const {InspectorControls, useBlockProps} = wp.blockEditor;
const ServerSideRender = wp.serverSideRender || wp.components.ServerSideRender;

const PokemonCard = BlockFactory.create(manifest, {
    edit: ((props) => {
        const {setAttributes, attributes} = props;
        const blockProps = useBlockProps();

        let {picks} = attributes;
        picks = picks === '' || typeof picks === 'undefined' ? [] : picks.split(',');

        return (
            <div {...blockProps}>
                <ServerSideRender block='vincentragosta/pokemon-card' attributes={attributes} />
                <InspectorControls>
                    <PanelBody title={__('Selected Pokemon Cards', 'vincentragosta')} key={'posts'}>
                        <PostPicker
                            isMulti={false}
                            initialSelection={picks}
                            postTypes={['pokemon-card']}
                            onChange={(picks) => {
                                setAttributes({picks});
                            }}
                        />
                    </PanelBody>
                </InspectorControls>
            </div>
        )
    })
});

export default PokemonCard;
