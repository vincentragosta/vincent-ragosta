function requireAll(r) {
    r.keys().forEach(r);
}
requireAll(require.context('../../images/', true, /\.(bmp|png|jpe?g|gif)$/i));
requireAll(require.context('../../images/svg/', true, /\.svg$/));
