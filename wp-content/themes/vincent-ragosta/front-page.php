<?php if (have_posts()): ?>
    <?php while (have_posts()): the_post(); global $post; ?>
        <article <?php post_class(); ?>>
            <div class="container-fluid">
                <?php if (!empty(get_the_content())): ?>
                    <?php the_content(); ?>
                <?php endif; ?>
            </div>
        </article>
    <?php endwhile; ?>
<?php else: ?>
    <div class="container-fluid">
        <div class="alert alert-warning">
            <?php _e(is_404() ?
                'Sorry, but the page you were trying to view does not exist.' :
                'Sorry, no results were found.', 'wp-scaffold');
            ?>
        </div>
    </div>
<?php endif; ?>
<?php the_posts_navigation(); ?>
